# v. 0.6.16

- changed how Karma tests are done
- added more error options
- added a few new options for Karma tests in the util folder
- made the boilerplate more ES2015 / ES2017 friendly
- updated dependencies

# v. 0.6.3

- `gulp bump` now automatically commit and push to git repo on version bump
- added two new tools for push and commit changes
- fixed issue with Circle
- fixed build issue

# v. 0.5.5

- changed how tests are running in the browser
- added a capitalizeFirstLetter for handling module names from package.json
- removed some legacy dependencies

# v. 0.5.3

- added Istanbul coverage report support for Karma

# v. 0.5.1

- renamed Gulp task `compress` to `gzip`

# v. 0.5.0

- fixed eslint configuration
- updated dependencies

# v. 0.4.5

- added a few new Gulp tasks
- fixed issues with the watch tasks
- added support for environment variabels
- updated readme

# v. 0.3.0

- Improved TypeScript (TS) support

# v. 0.2.8

- replaced Isparta with latest Istanbul for the coverage report.

# v. 0.2.3

- added option to gZip bundled files - `gulp compress`

# v. 0.2.2

- addede support for Google Closure Compiler

# v. 0.1.2

- added extended support for TypeScript
- added linting for TypeScript source files
- added a gulp taks to handle TypeScript tests
- simplified Travis
- improved Karma tests

# v. 0.1.0

- added support for Typescript in unit tests

# v. 0.0.3

- added support for bundling Typescript down to ES5

# v. 0.0.2

- added missing Gulp tasks such as `watch:chrome` and `watch:phantom`
- re-factored how the bundle process works
- added tools for multi-bundling
- added tools for running sequenses

# v. 0.0.1

- first release