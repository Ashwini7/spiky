import path from 'path';
import rollup from 'rollup-stream';
import babel from 'rollup-plugin-babel';
import uglify from 'rollup-plugin-uglify';
import nodeResolve from 'rollup-plugin-node-resolve';
import commonjsPlugin from 'rollup-plugin-commonjs';
import replace from 'rollup-plugin-replace';
import jsonPlugin from 'rollup-plugin-json';
import filesize from 'rollup-plugin-filesize';
import typescript from 'rollup-plugin-typescript';
import capitalizeFirstLetter from './capitalizeFirstLetter';
import pkg from '../../../package.json';

/*
 * Banner
 **/
const copyright =
	'/*!\n' +
	' * ' + pkg.name + ' v' + pkg.version + '\n' +
	' * (c) ' + new Date().getFullYear() + ' ' + pkg.author.name + '\n' +
	' * Released under the ' + pkg.license + ' License.\n' +
	' */';

function bundle(format, entry) {

	return rollup({
		entry: path.resolve(entry),
		sourceMap: false,
		banner: copyright,
		plugins: [
			process.env.min === 'true' ? uglify({
				warnings: false,
				compress: {
					screw_ie8: true,
					dead_code: true,
					unused: true,
					keep_fargs: false,
					drop_debugger: true
				},
				mangle: {
					screw_ie8: true
				}
			}) : {},
			jsonPlugin(),
			// This plugin 'must' be listed before Babel.
			typescript(),
			babel({
				babelrc: false,
				exclude: 'node_modules/**',
				presets: ['es2015-rollup','stage-0'],
				plugins: process.env.NODE_ENV ?
					['transform-inline-environment-variables'] :
					[],
				sourceMaps: false
			}),
			nodeResolve({
				// use "jsnext:main" if possible
				// – see https://github.com/rollup/rollup/wiki/jsnext:main
				jsnext: true,
				// use "main" field or index.js, even if it's not an ES6 module
				// (needs to be converted from CommonJS to ES6
				// – see https://github.com/rollup/rollup-plugin-commonjs
				main: true,
				// if there's something your bundle requires that you DON'T
				// want to include, add it to 'skip'
				skip: [],
				// some package.json files have a `browser` field which
				// specifies alternative files to load for people bundling
				// for the browser. If that's you, use this option, otherwise
				// pkg.browser will be ignored
				browser: true
			}),
			commonjsPlugin(),
			filesize(),
			replace({
				'process.env.NODE_ENV': JSON.stringify('production'),
				VERSION: pkg.version
			})
		],
		format: format,
		moduleName: capitalizeFirstLetter(pkg.name),
		moduleId: false
	});
}

export default bundle;