import gulp from 'gulp';
import runKarma from '../util/runKarma';

gulp.task('test:chrome', runKarma(true, {
		singleRun: true,
		browsers: ['Chrome']
	}
));